#include <Wire.h>
const int MPU_ADDR = 0x68;  // Endereço I2C da MPU6050

void setup() {
  Wire.begin();        // Inicializa a comunicação I2C
  Serial.begin(9600);  // Inicializa a comunicação serial
  initializeMPU6050(); // Inicializa a MPU6050
}

void loop() {
  int16_t accelerometerX, accelerometerY, accelerometerZ;
  int16_t gyroX, gyroY, gyroZ;
  
  // Lê os valores do acelerômetro e giroscópio
  accelerometerX = readData(MPU_ADDR, 0x3B);
  accelerometerY = readData(MPU_ADDR, 0x3D);
  accelerometerZ = readData(MPU_ADDR, 0x3F);
  gyroX = readData(MPU_ADDR, 0x43);
  gyroY = readData(MPU_ADDR, 0x45);
  gyroZ = readData(MPU_ADDR, 0x47);
  
  // Converte os valores de aceleração para fração de gravidade (g)
  float accelerationX_g = convertToGravity(accelerometerX);
  float accelerationY_g = convertToGravity(accelerometerY);
  float accelerationZ_g = convertToGravity(accelerometerZ);
  
  // Converte os valores de velocidade angular para graus/s
  float gyroX_degPerSec = convertToDegreesPerSecond(gyroX);
  float gyroY_degPerSec = convertToDegreesPerSecond(gyroY);
  float gyroZ_degPerSec = convertToDegreesPerSecond(gyroZ);
  
  // Imprime os valores na porta serial
  Serial.print("--------------------------Amostra--------------------\n");
  Serial.print("Aceleracao (g): X =      ");
  Serial.print(accelerationX_g);
  Serial.print("| Y =      ");
  Serial.print(accelerationY_g);
  Serial.print("| Z =      ");
  Serial.println(accelerationZ_g);
  //---------------Após a calibração--------------------
//  Serial.print("--------------------------Calibrado--------------------\n");
  Serial.print("Aceleracao (g): X_cali = ");
  Serial.print(accelerationX_g*1.01-0.02);
  Serial.print("| Y_cali = ");
  Serial.print(accelerationY_g*0.995-0.015);
  Serial.print("| Z_cali = ");
  Serial.println(accelerationZ_g*1.025+0.015);

  
  delay(1000);  // Intervalo de leitura dos dados
}

void initializeMPU6050() {
  // Inicializa a MPU6050
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B);  // Registrador PWR_MGMT_1
  Wire.write(0);     // Acorda a MPU6050 (seta o bit 6 em 0)
  Wire.endTransmission(true);
}

int16_t readData(int deviceAddress, byte regAddress) {
  // Lê 2 bytes de dados do registrador especificado
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission(false);
  
  Wire.requestFrom(deviceAddress, 2, true);
  byte highByte = Wire.read();
  byte lowByte = Wire.read();
  
  int16_t value = (highByte << 8) | lowByte;
  return value;
}

float convertToGravity(int16_t value) {
  // Converte o valor do acelerômetro para fração de gravidade (g)
  float gravity = value / 16384.0;  // Valor máximo de 16 bits é 32767, então dividimos por 16384 para obter o valor em g
  
  return gravity;
}

float convertToDegreesPerSecond(int16_t value) {
  // Converte o valor do giroscópio para graus/s
  float degPerSec = value / 131.0;  // Valor máximo de 16 bits é 32767, então dividimos por 131 para obter o valor em graus/s
  return degPerSec;
}
