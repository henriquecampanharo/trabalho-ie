#include <Wire.h>
const int MPU_ADDR = 0x68;  // Endereço I2C da MPU6050
float somaX = 0,somaY = 0,somaZ = 0;
float mediaX,mediaY,mediaZ;
int flag = 0, cont = 0, primeiro = 1;

void setup() {
  Wire.begin();        // Inicializa a comunicação I2C
  Serial.begin(9600);  // Inicializa a comunicação serial
  initializeMPU6050(); // Inicializa a MPU6050
}

void loop() {
  char leitura = '0';
  if(Serial.available() > 0){
    Serial.println("Entrada de valor");
    leitura = Serial.read();
    Serial.println(leitura);
    if(leitura == 'x'){
      flag = 1;
    }else if(leitura == 'y'){
      flag = 2;
    }else if(leitura == 'z'){
      flag = 3;
    }
  }
  if(flag == 1){ //Faz a leitura da dédia do x
    Serial.println("--------------------------Fazendo medidas de X----------------------------");
    for(cont = 1; cont<= 100; cont ++){
      int16_t accelerometerX, accelerometerY, accelerometerZ;
      int16_t gyroX, gyroY, gyroZ;
      
      // Lê os valores do acelerômetro e giroscópio
      accelerometerX = readData(MPU_ADDR, 0x3B);
      
      // Converte os valores de aceleração para fração de gravidade (g)
      float accelerationX_g = convertToGravity(accelerometerX);
      
      // Imprime os valores na porta serial
      Serial.print("Aceleracao (g): X = ");
      Serial.println(accelerationX_g);
      
      delay(100);  // Intervalo de leitura dos dados
      
      somaX = somaX + (float)accelerationX_g;
      
      if(cont == 100){
        mediaX = somaX/100;
        flag = 0;
        Serial.println("Media do eixo X: ");
        Serial.println(mediaX);
        primeiro = 1;
      }
    }
}else if(flag == 2){ //Faz a leitura da dédia do y
  Serial.println("--------------------------Fazendo medidas de Y----------------------------");
      for(cont = 1; cont<= 100; cont ++){
        
      int16_t accelerometerX, accelerometerY, accelerometerZ;
      int16_t gyroX, gyroY, gyroZ;
      
      // Lê os valores do acelerômetro e giroscópio
      accelerometerY = readData(MPU_ADDR, 0x3D);
 
      // Converte os valores de aceleração para fração de gravidade (g)

      float accelerationY_g = convertToGravity(accelerometerY);
     
      // Imprime os valores na porta serial
      Serial.print("Aceleracao (g): Y = ");
      Serial.println(accelerationY_g);
      
      delay(100);  // Intervalo de leitura dos dados
      
      somaY = somaY + (float)accelerationY_g;
      
      if(cont == 100){
        mediaY = somaY/100;
        flag = 0;
        Serial.println("Media do eixo Y: ");
        Serial.println(mediaY);
        primeiro = 1;
      }
    }
}else if(flag==3){ //Faz a leitura da dédia do Z
  Serial.println("--------------------------Fazendo medidas de Z----------------------------");
      for(cont = 1; cont<= 100; cont ++){
      int16_t accelerometerX, accelerometerY, accelerometerZ;
      int16_t gyroX, gyroY, gyroZ;
      
      // Lê os valores do acelerômetro e giroscópio
      accelerometerZ = readData(MPU_ADDR, 0x3F);
      // Converte os valores de aceleração para fração de gravidade (g)
      float accelerationZ_g = convertToGravity(accelerometerZ);
      
      // Imprime os valores na porta serial
      Serial.print("Aceleracao (g): Z = ");
      Serial.println(accelerationZ_g);
      
      delay(100);  // Intervalo de leitura dos dados
      
      somaZ = somaZ + (float)accelerationZ_g;
      
      if(cont == 100){
        mediaZ = somaZ/100;
        flag = 0;
        Serial.println("Media do eixo Z: ");
        Serial.println(mediaZ);
        primeiro = 1;
      }
    }
}else{
  if(primeiro == 1){
    Serial.println("Aguardando nova entrada, x,y,z.");
    primeiro = 0;
  }
  delay(1000);
}
}
void initializeMPU6050() {
  // Inicializa a MPU6050
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B);  // Registrador PWR_MGMT_1
  Wire.write(0);     // Acorda a MPU6050 (seta o bit 6 em 0)
  Wire.endTransmission(true);
}

int16_t readData(int deviceAddress, byte regAddress) {
  // Lê 2 bytes de dados do registrador especificado
  Wire.beginTransmission(deviceAddress);
  Wire.write(regAddress);
  Wire.endTransmission(false);
  
  Wire.requestFrom(deviceAddress, 2, true);
  byte highByte = Wire.read();
  byte lowByte = Wire.read();
  
  int16_t value = (highByte << 8) | lowByte;
  return value;
}

float convertToGravity(int16_t value) {
  // Converte o valor do acelerômetro para fração de gravidade (g)
  float gravity = value / 16384.0;  // Valor máximo de 16 bits é 32767, então dividimos por 16384 para obter o valor em g
  return gravity;
}

float convertToDegreesPerSecond(int16_t value) {
  // Converte o valor do giroscópio para graus/s
  float degPerSec = value / 131.0;  // Valor máximo de 16 bits é 32767, então dividimos por 131 para obter o valor em graus/s
  return degPerSec;
}
