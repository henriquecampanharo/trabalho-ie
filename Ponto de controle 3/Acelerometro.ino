// Librerias I2C para controlar el mpu6050
// la libreria MPU6050.h necesita I2Cdev.h, I2Cdev.h necesita Wire.h
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

// La dirección del MPU6050 puede ser 0x68 o 0x69, dependiendo 
// del estado de AD0. Si no se especifica, 0x68 estará implicito
MPU6050 sensor;
//Eixo Y
//[0,10,20,30,40,50,60,70]
//[6.9,16.5,26.47,35.63,46.60,55.45,63.97,73.60]
// Valores RAW (sin procesar) del acelerometro  en los ejes x,y,z
int ax, ay, az;
float somador =0.0;
void setup() {
  Serial.begin(115200);    //Iniciando puerto serial
  Wire.begin();           //Iniciando I2C  
  sensor.initialize();    //Iniciando el sensor

  if (sensor.testConnection()) Serial.println("Sensor iniciado correctamente");
  else Serial.println("Error al iniciar el sensor");
}

void loop() {
  Serial.println("Mova para a proxima posição/n");
  delay(5000);
  
  for(int i=0;i<100;i++){ 

  sensor.getAcceleration(&ax, &ay, &az);

  float accel_ang_y=atan(ax/sqrt(pow(ay,2) + pow(az,2)))*(180.0/3.14);
  float accel_ang_x=atan(ay/sqrt(pow(ax,2) + pow(az,2)))*(180.0/3.14);
  Serial.print("Inclinação Y:");
  Serial.println(accel_ang_y);
  somador=somador+accel_ang_y;
  delay(10);
  
  }
  Serial.print(somador/100);
  somador = 0.0;
}
