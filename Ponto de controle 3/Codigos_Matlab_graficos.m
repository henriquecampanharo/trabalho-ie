% Fechar gráficos anteriores
close all;

% Pontos de referência
y_ref = [0, 10, 20, 30, 40, 50, 60, 70];

% Valores obtidos experimentalmente
%y_exp = [6.9, 16.5, 26.47, 35.63, 46.60, 55.45, 63.97, 73.60];
y_exp = [1.03,5.53,7.53,10.12,12.59,18.92,20.59,27.31];

% Cálculo do erro
erro = y_ref - y_exp;

% Vetor para armazenar os valores do REMQ
rmse = zeros(1, length(erro));

% Print do REMQ para cada medida
for i = 1:length(erro)
    rmse(i) = sqrt(mean(erro(i)^2));
    fprintf('REMQ para a medida %d: %.2f\n', i, rmse(i));
end

% Plot dos gráficos interpolados
figure;
x = 0:length(y_ref)-1;
x_interpolado = linspace(0, length(y_ref)-1, 1000);
y_interpolado_ref = interp1(x, y_ref, x_interpolado, 'spline');
y_interpolado_exp = interp1(x, y_exp, x_interpolado, 'spline');
plot(x_interpolado, y_interpolado_ref, 'b-', 'LineWidth', 1.5);
hold on;
plot(x, y_ref, 'ro', 'MarkerSize', 6);
plot(x_interpolado, y_interpolado_exp, 'g-', 'LineWidth', 1.5);
plot(x, y_exp, 'gx', 'MarkerSize', 6);
hold off;
legend('Interpolado (Referência)', 'Valor de referência', 'Interpolado (Experimental)', 'Valor experimental');
xlabel('Amostras');
ylabel('Ângulo');
title('Gráfico Interpolado e Valores Experimentais Giroscópio');
ylim([-10, 80]);
yticks(0:5:80);
grid on;

% Plot do gráfico de erro
figure;
plot(x, erro, 'ro-', 'LineWidth', 1.5);
xlabel('Amostras');
ylabel('Erro');
title('Gráfico de Erro Giroscópio');
grid on;
