// Librerias I2C para controlar el mpu6050
//Para o giroscópio é necessário aplicar um filtro para que o sistema não incremente sem limites, sendo esse o filtro de complemento, que utiliza valores do acelenômtro para auxiliar na d=coleta dos dados do giroscópio, sendo essa uma medida mais precisa
// la libreria MPU6050.h necesita I2Cdev.h, I2Cdev.h necesita Wire.h
//Eixo Y
//[0,10,20,30,40,50,60,70]
//[1.03,5.53,7.53,10.12,12.59,18.92,20.59,27.31]
#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

// La dirección del MPU6050 puede ser 0x68 o 0x69, dependiendo 
// del estado de AD0. Si no se especifica, 0x68 estará implicito
MPU6050 sensor;

// Valores RAW (sin procesar) del acelerometro y giroscopio en los ejes x,y,z
int gx, gy, gz;

long tiempo_prev, dt;
float girosc_ang_x, girosc_ang_y, somador =0.0;
float girosc_ang_x_prev, girosc_ang_y_prev;

void setup() {
  Serial.begin(115200);    //Iniciando puerto serial
  Wire.begin();           //Iniciando I2C  
  sensor.initialize();    //Iniciando el sensor

  if (sensor.testConnection()){ 
    Serial.println("Sensor iniciado correctamente");
  }else{ 
    Serial.println("Error al iniciar el sensor");
  }
  tiempo_prev=millis();
}

void loop() {
  // Leer las velocidades angulares
  Serial.println("Mova para a proxima posição");
  delay(5000);
  for(int i=0;i<100;i++){ 
  sensor.getRotation(&gx, &gy, &gz);
  
  //Calcular los angulos rotacion:
  
  dt = millis()-tiempo_prev;
  tiempo_prev=millis();
  
  girosc_ang_x = (gx/131)*dt/1000.0 + girosc_ang_x_prev;
  girosc_ang_y = (gy/131)*dt/1000.0 + girosc_ang_y_prev;

  girosc_ang_x_prev=girosc_ang_x;
  girosc_ang_y_prev=girosc_ang_y;


  Serial.print("Rotação em Y: ");
  Serial.println(girosc_ang_y);
  somador=somador+girosc_ang_y;
  delay(100);
  }
  Serial.print(somador/100);
  somador = 0.0;
}
